#include "remoteservercontrol.h"

#include <fstream>
#include <iostream>

#include <grpc/grpc.h>
#include <grpc++/server.h>
#include <grpc++/channel.h>
#include <grpc++/create_channel.h>
#include <grpc++/client_context.h>
#include <grpc++/server_builder.h>
#include <grpc++/server_context.h>
#include <grpc++/security/credentials.h>
#include <grpc++/security/server_credentials.h>

using grpc::Server;
using grpc::ServerBuilder;
using grpc::ServerContext;
using grpc::ServerReader;
using grpc::ServerWriter;
using grpc::Status;
using grpc::StatusCode;

#include "debug.h"
#include "tools/processops.h"
#include "tools/fileops.h"

#define MAX_DATA_SPEED 1024 * 1024 * 2

static inline std::vector<std::string> split(const std::string& s, char delimiter, bool keepEmpty = true)
{
	std::vector<std::string> tokens;
	std::string token;
	std::istringstream tokenStream(s);
	while (std::getline(tokenStream, token, delimiter))
	{
		if (keepEmpty || token.size())
			tokens.push_back(token);
	}
	return tokens;
}

RemoteServerControl::RemoteServerControl()
{

}

int RemoteServerControl::startAndWait(int port)
{
	std::string ep("0.0.0.0:");
	ep = ep + std::to_string(port);
	gWarn("Starting service %s", ep.data());
	ServerBuilder builder;
	builder.AddListeningPort(ep, grpc::InsecureServerCredentials());
	builder.RegisterService(this);
	// TODO add new services
	std::unique_ptr<Server> server(builder.BuildAndStart());
	server->Wait();
	return 0;
}

grpc::Status RemoteServerControl::GetFirmware(grpc::ServerContext *context, const google::protobuf::Empty *request, remotehelper::GetFirmwareResponse *response)
{
	(void)context, (void)request;
	response->set_version_string("1.0.1");
	return grpc::Status::OK;
}

grpc::Status RemoteServerControl::RunCommand(grpc::ServerContext *context, ::grpc::ServerReaderWriter<remotehelper::CommandRunResponse, remotehelper::CommandRunRequest> *stream)
{
	(void)context;
	remotehelper::CommandRunRequest req;
	while(stream->Read(&req)) {
		remotehelper::CommandRunResponse resp;
		runCommand(&req, &resp);
		stream->Write(resp);
	}
	return grpc::Status::OK;
}

grpc::Status RemoteServerControl::RunSingleCommand(grpc::ServerContext *context, const remotehelper::CommandRunRequest *request, remotehelper::CommandRunResponse *response)
{
	(void)context;
	runCommand(request, response);
	return grpc::Status::OK;
}

grpc::Status RemoteServerControl::Upload(grpc::ServerContext *context, ::grpc::ServerReader<remotehelper::UploadRequest> *reader, remotehelper::UploadResponse *response)
{
	(void)context;
	remotehelper::UploadRequest req;
	FileOps fops;
	int err = 0;
	while (reader->Read(&req)) {
		std::string name = req.remote_name();
		if (!fops.isOpen())
			fops.open(name.data(), true);
		std::string context = req.context();
		err = fops.append(context);
		if (err < 0)
			break;
	}
	if (err < 0) {
		response->set_message("File operation halted");
		response->set_code(remotehelper::FAILED);
	} else
		response->set_code(remotehelper::OK);
	fops.close();
	return grpc::Status::OK;
}

grpc::Status RemoteServerControl::Download(grpc::ServerContext *context, const remotehelper::DownloadRequest *request, ::grpc::ServerWriter<remotehelper::DownloadResponse> *writer)
{
	int err = downloadBytePerByte(request, writer);
	if (err)
		return grpc::Status::CANCELLED;
	return grpc::Status::OK;
#if 0
	(void)context;
	std::string name = request->remote_name();
	FileOps fops;
	fops.open(name.data());
	std::string data = fops.read();
	fops.close();
	int length = data.size();
	const char *bytes = data.data();
	remotehelper::DownloadResponse resp;
	remotehelper::DownloadProgress *progress = resp.mutable_progress();
	progress->set_max_value(length -1);
	progress->set_progress_name("MinaKodukBulentBasqannn");
	int reLength = length;
	int mod = length / MAX_DATA_SPEED;
	for(int i = 0; i < mod + 1; i++) {
		if (reLength < MAX_DATA_SPEED) {
			resp.set_context(bytes, reLength);
			progress->set_current_value(length);
		} else {
			char t[MAX_DATA_SPEED];
			strncpy(t, bytes, MAX_DATA_SPEED);
			resp.set_context(t, MAX_DATA_SPEED);
			progress->set_current_value(i * MAX_DATA_SPEED);
		}
		if (!writer->Write(resp))
			return grpc::Status::CANCELLED;
		bytes += MAX_DATA_SPEED;
		reLength -= MAX_DATA_SPEED;
	}
	return grpc::Status::OK;
#endif
}

int RemoteServerControl::downloadBytePerByte(const remotehelper::DownloadRequest *request, ::grpc::ServerWriter<remotehelper::DownloadResponse> *writer)
{
	std::string name = request->remote_name();
	FileOps fops;
	fops.open(name);
	remotehelper::DownloadResponse resp;
	remotehelper::DownloadProgress *progress = resp.mutable_progress();
	progress->set_max_value(fops.fileLength() - 1);
	progress->set_progress_name(name.data());
	std::string data;
	int cnt = 0;
	while (!(fops.read(&data, MAX_DATA_SPEED) < 0)) {
		resp.set_context(data.data(), data.size());
		cnt = cnt + data.size();
		progress->set_current_value(cnt);
		if (!writer->Write(resp))
			return -1;
	}
	fops.close();
	return 0;
}

void RemoteServerControl::runCommand(const remotehelper::CommandRunRequest *request, remotehelper::CommandRunResponse *response)
{
	(void)request->pwd(); // TODO whats can i do with this variable
	std::string cmd = request->command_name();
	if (request->arguments_size() > 0) {
		for (int i = 0; i < request->arguments_size(); i++) {
			if (request->arguments(i).empty())
				continue;
			cmd = cmd + " " + request->arguments(i);
		}
	}
	ProcessOps pops(cmd);
	int err = pops.run();
	if (err)
		response->set_err_code(remotehelper::FAILED);
	else {
		response->set_err_code(remotehelper::OK);
		if (pops.isCoreDump())
			response->set_err_code(remotehelper::UNKNOWN);
	}
	response->set_stdout(pops.stdout());
	response->set_stderr(pops.stderr());
	response->set_err_code(err);
	return;
}

grpc::Status RemoteServerControl::ListFiles(grpc::ServerContext *context, const remotehelper::EmptyQ *request, ::grpc::ServerWriter<remotehelper::FileQ> *writer)
{
	(void)context;
	(void)request;

	ProcessOps pops("ls -ld *");
	int err = pops.run();
	if (err)
		return grpc::Status(StatusCode::ABORTED, "");

	std::vector<std::string> results = split(pops.stdout().data(), '\n', false);

	int i = 0;
	for (auto itr = results.begin(); itr != results.end(); itr++) {
		std::string row = *itr;
		std::vector<std::string> columns = split(row, ' ', false);

		if (columns.size() != 9)
			continue;

		remotehelper::FileQ f;
		f.set_id(i++);
		f.set_isdir((columns.at(0).at(0) == 'd'));
		f.set_name(columns.at(8));
		f.set_path("./");
		f.set_permissions(columns.at(0));
		int number = std::atoi (columns.at(4).c_str());
		f.set_file_size(number);
		f.set_full_info(row);

		writer->Write(f);
	}

	return grpc::Status(StatusCode::OK, "");
}
