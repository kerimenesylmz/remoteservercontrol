#include "processops.h"

#include "debug.h"

ProcessOps::ProcessOps(const std::string cmd)
{
	command = cmd + " 2>&1";
}

int ProcessOps::run()
{
	std::array<char, 128> buffer;
	pipe = popen(command.data(), "r");
	if (!pipe) {
		gWarn("popen() '%s'' failed!", command.data());
		return -1;
	}
	result.clear();
	while (fgets(buffer.data(), buffer.size(), pipe) != nullptr)
		result += buffer.data();
	auto exitCode = pclose(pipe);
	parseError(exitCode);
	gLog("Command completed succesfuly:"
		 "cmd:'%s' stdout:'%s' stderr:'%d' killedSignalNumer:'%d' isCoreDump:'%s'",
		 command.data(), stdout().data(), stderr(), signalNumber(), isCoreDump() ? "true" : "false"
			);
	return 0;
}

void ProcessOps::parseError(int error)
{
	errorCode = (error >> 8) & 0xFF;
	signalCode = error & 0x007F;
	coreDumpSignal = error & 0x0080;
}



