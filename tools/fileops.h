#ifndef FILEOPS_H
#define FILEOPS_H

#include <fstream>

class FileOps
{
public:
	FileOps();
	int open(const std::__cxx11::string name, bool truncate = false);
	int close();
	int append(std::string bytes);

	std::string read();
	std::string read(int byteSize);
	int read(std::string *data, int byteSize);

	int fileLength();
	bool isOpen() {return fs.is_open();}

protected:
	std::fstream fs;
	std::string name;
	int lastOffset;
};

#endif // FILEOPS_H
