#include "fileops.h"
#include "debug.h"

FileOps::FileOps()
{
	lastOffset = 0;
}

int FileOps::open(const std::string name, bool truncate)
{
	auto mode = std::fstream::in | std::fstream::out;
	if (truncate)
		mode = mode | std::fstream::trunc;
	this->name = name;
	if (!isOpen())
		fs.open(name,  mode);
	return 0;
}

int FileOps::close()
{
	if (isOpen())
		fs.close();
	return 0;
}

int FileOps::append(std::string bytes)
{
	if (!fs.is_open())
		return -1;
	gLog("Writing file '%s', size %d", name.data(), bytes.size());
	fs.write(bytes.data(), bytes.size());
	return bytes.size();
}

std::string FileOps::read()
{
	if (!isOpen())
		return std::string("");
	fs.seekg(0, fs.end);
	int length = fs.tellg();
	fs.seekg(0, fs.beg);
	gLog("Reading file '%s'', size '%d'", name.data(), length);
	char *buffer = new char[length];
	fs.read(buffer, length);
	std::string bytes = std::string(buffer, length);
	return bytes;
}

std::string FileOps::read(int byteSize)
{
	if (!isOpen())
		return std::string("");
	if (lastOffset >= fileLength())
		return std::string("");
	if (lastOffset + byteSize > fileLength())
		byteSize = fileLength() - lastOffset;
	fs.seekg(lastOffset, fs.beg);
	gLog("file in position %d, byteSize %d", lastOffset, byteSize);
	char buffer[byteSize];
	fs.read(buffer, byteSize);
	std::string bytes = std::string(buffer, byteSize);
	lastOffset += byteSize;
	return bytes;
}

int FileOps::read(std::string *data, int byteSize)
{
	std::string tmp = read(byteSize);
	if (tmp.empty())
		return -1;
	*data = tmp;
	return 0;
}

int FileOps::fileLength()
{
	if (!isOpen())
		return -1;
	fs.seekg(0, fs.end);
	int length = fs.tellg();
	fs.seekg(0, fs.beg);
	return length;
}

