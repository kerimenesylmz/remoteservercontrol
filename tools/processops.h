#ifndef PROCESSOPS_H
#define PROCESSOPS_H

#include <string>
#include <array>
class ProcessOps
{
public:
	ProcessOps(const std::string cmd);
	int run();

	std::string stdout() {return result;}
	int stderr() {return errorCode;}
	int signalNumber() {return signalCode;}
	bool isCoreDump() {return coreDumpSignal ? true : false;}
protected:
	void parseError(int error);
private:
	FILE *pipe;
	std::string command;
	std::string result;
	int errorCode;
	int signalCode;
	int coreDumpSignal;
};

#endif // PROCESSOPS_H
