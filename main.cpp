#include <remoteservercontrol.h>
#include "debug.h"
#include <iostream>

#include <grpc/grpc.h>
#include <grpc++/server.h>
#include <grpc++/channel.h>
#include <grpc++/create_channel.h>
#include <grpc++/client_context.h>
#include <grpc++/server_builder.h>
#include <grpc++/server_context.h>
#include <grpc++/security/credentials.h>
#include <grpc++/security/server_credentials.h>

using grpc::Server;
using grpc::ServerBuilder;
using grpc::ServerContext;
using grpc::ServerReader;
using grpc::ServerWriter;
using grpc::Status;
using grpc::StatusCode;
#include <proto/remotehelper.grpc.pb.h>

#include <fstream>
#include <unistd.h>

static char * getBytesFile(const char *filename, int *size)
{
	std::ifstream file (filename, std::ifstream::binary);
	if (file) {
		file.seekg(0, file.end);
		int length = file.tellg();
		*size = length;
		file.seekg(0, file.beg);
		char *buffer = new char [length];
		file.read(buffer, length);
		file.close();
		return buffer;
	}
}

int testGrpcUploadFile(const char *filename)
{
	int size;
	char *bytes = getBytesFile(filename, &size);

	std::string ep = "127.0.0.1:56001";
	std::shared_ptr<grpc::Channel> chn = grpc::CreateChannel(ep, grpc::InsecureChannelCredentials());
	std::shared_ptr<remotehelper::ServerService::Stub> stub = remotehelper::ServerService::NewStub(chn);
	grpc::ClientContext ctx;
	remotehelper::UploadResponse upResp;
	std::unique_ptr< ::grpc::ClientWriter< ::remotehelper::UploadRequest>> writer = stub->Upload(&ctx, &upResp);

	for (int i = 0; i < size ; i++) {
		remotehelper::UploadRequest req;
		req.set_remote_name("test2.txt");
		const char t = bytes[i];
		req.set_context(&t, 1);
		writer->Write(req);
		usleep(1);
	}
	return 0;
}

int testGrpcDownloadFile(const char *filename)
{
	std::string ep = "127.0.0.1:56001";
	std::shared_ptr<grpc::Channel> chn = grpc::CreateChannel(ep, grpc::InsecureChannelCredentials());
	std::shared_ptr<remotehelper::ServerService::Stub> stub = remotehelper::ServerService::NewStub(chn);
	grpc::ClientContext ctx;
	remotehelper::DownloadRequest req;
	req.set_remote_name(filename);
	std::unique_ptr< ::grpc::ClientReader< ::remotehelper::DownloadResponse>> reader = stub->Download(&ctx, req);
	remotehelper::DownloadResponse resp;
	while (reader->Read(&resp)) {
		gWarn("resp %d size ", resp.context().size());
		gWarn("Progress max VAlue %d", resp.progress().max_value());
		gWarn("Progress current VAlue %d", resp.progress().current_value());
		gWarn("%s", resp.progress().progress_name().data());
	}
}

int testExecCommand(const char *cmd, const char *arg1, const char *arg2)
{
	std::string ep = "127.0.0.1:56001";
	std::shared_ptr<grpc::Channel> chn = grpc::CreateChannel(ep, grpc::InsecureChannelCredentials());
	std::shared_ptr<remotehelper::ServerService::Stub> stub = remotehelper::ServerService::NewStub(chn);
	grpc::ClientContext ctx;
	remotehelper::CommandRunRequest req;
	remotehelper::CommandRunResponse resp;
	req.set_command_name(cmd);
	std::string *s = req.add_arguments();
	*s = arg1;
	std::string *s2 = req.add_arguments();
	*s2 = arg2;
	stub->RunSingleCommand(&ctx, req, &resp);
	gWarn("STDOUT: '%s'", resp.stdout().data());
	gWarn("STDERROR : '%d'", resp.stderr());
	return 0;
}

int main(int argc, char *argv[])
{
	loguru::g_stderr_verbosity = 7;
	char *var = getenv("DEBUG");
	if (var)
		loguru::g_stderr_verbosity = std::stoi(var);
	if (argc < 2) {
		RemoteServerControl *remote = new RemoteServerControl();
		remote->startAndWait(56001);
		return 0;
	}

	std::string arg = argv[1];
	if (arg.find("upload") != std::string::npos) {
		return testGrpcUploadFile("test.txt");
	}

	if (arg.find("download") != std::string::npos) {
		return testGrpcDownloadFile("test.txt");
	}

	if (arg.find("exec") != std::string::npos) {
		return testExecCommand(argv[2], "", "");
	}

	return 0;
}

