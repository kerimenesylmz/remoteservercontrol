#ifndef REMOTESERVERCONTROL_H
#define REMOTESERVERCONTROL_H

#include <proto/remotehelper.grpc.pb.h>

class GrpcThread;
class RemoteServerControl : public remotehelper::ServerService::Service
{
public:
	RemoteServerControl();
	int startAndWait(int port);
	::grpc::Status GetFirmware(grpc::ServerContext *context, const google::protobuf::Empty *request, remotehelper::GetFirmwareResponse *response);
	::grpc::Status RunCommand(grpc::ServerContext *context, ::grpc::ServerReaderWriter<remotehelper::CommandRunResponse, remotehelper::CommandRunRequest> *stream);
	::grpc::Status RunSingleCommand(grpc::ServerContext *context, const remotehelper::CommandRunRequest *request, remotehelper::CommandRunResponse *response);
	::grpc::Status Upload(grpc::ServerContext *context, ::grpc::ServerReader<remotehelper::UploadRequest> *reader, remotehelper::UploadResponse *response);
	::grpc::Status Download(grpc::ServerContext *context, const remotehelper::DownloadRequest *request, ::grpc::ServerWriter<remotehelper::DownloadResponse> *writer);

	::grpc::Status ListFiles(grpc::ServerContext *context, const remotehelper::EmptyQ *request, ::grpc::ServerWriter<remotehelper::FileQ> *writer);
protected:
	void runCommand(const remotehelper::CommandRunRequest *request, remotehelper::CommandRunResponse *response);
	int downloadBytePerByte(const remotehelper::DownloadRequest *request, ::grpc::ServerWriter<remotehelper::DownloadResponse> *writer);
};

#endif // REMOTESERVERCONTROL_H
