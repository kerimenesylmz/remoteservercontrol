#QT += core
#QT -= gui

TARGET = RemoteServerControl
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

TEMPLATE = app

include (proto/proto.pri)
include (tools/tools.pri)

SOURCES += main.cpp \
    remoteservercontrol.cpp \
    loguru.cpp

HEADERS += \
    remoteservercontrol.h \
    debug.h \
    loguru.hpp

LIBS += -lpthread -ldl
DEFINES += LOGURU_WITH_STREAMS

